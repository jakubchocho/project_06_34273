<h1 align="center">

<br>

<p align="center">
Quiz
</p>

</h1>

## Opis

<p>Program pozwala użytkownikowi na sprawdzenie swojej wiedzy poprzez odpowiedź na kilka pytań z których każde posiada cztery wiarianty odpowiedzi, z czego tylko jedna jest poprawna. Za każdą poprawną odpowiedź użytkownik uzyskuje jeden punkt. Po zakończeniu rozgrywki program generuje raport, w którym użytkownik może sprawdzić poprawność swoich odpowiedzi. W raporcie znajduje się również informacja, czy użytkownikowi udało się zaliczyć test.</p>

<br>

## Opis funkcjonalny

<h3>Ekran pytania</h3>

<br>

<p>1. Treść pytania</p>
<p>2. Liczba zdobytych punktów, aktualizowanych na bieżąco</p>
<p>3. Odpowiedzi możliwe do zaznaczenia</p>
<p>4. Zatwierdzenie odpowiedzi i przejście do nastepnego pytania</p>

<img src="images/screenshot1.png">

<p>Użytkownik wybiera tylko jedną odpowiedź z możliwych na podane pytanie oraz ją zatwierdza. Po zatwierdzeniu odpowiedzi wyświetla się nowe pytanie wraz z dostępnymi odpowiedziami. Gdy pytania się skończą, zostaje wyświetlony ekran końcowy.</p>

<br>

<h3>Ekran wynikowy</h3>

<br>

<p>5. Informacja o wyniku zdającego</p>
<p>6. Punkty końcowe</p>
<p>7. Przycisk kończy program i generuje raport z wynikami w formacie tekstowym</p>

<img src="images/screenshot2.png">

<p>Po odpowiedzeniu na wszystkie pytania zostaje wyświetlona informacja czy wynik testu jest pozytywny lub negatywny. Zostają wyświetlone także zdobyte punkty oraz ile było maksymalnie do zdobycia.</p>

<br>

<h3>Raport z wynikami</h3>

<br>

<img src="images/screenshot3.png">

<p>Po zakończeniu programu zostaje wygenerowany plik tekstowy z wynikami. Można zobaczyć jakich odpowiedzi udzielił użytkownik oraz jakie były prawidłowe.</p>

<br>

## Użyte narzędzia
<p>IntelliJ IDEA</p>
<p>sdk 17</p>
<p>javafx - interfejs użytkownika</p>
<p>opencsv-2.2 - wczytywanie danych z pliku csv</p>

<br>

## Podział pracy

| Jakub Chochołowicz | Bartosz Niziołek |
| -------------------| ---------------- |
| Klasa do pytań - Question.java | Pytania do aplikacji - questions.csv |
| Podstawowy interfejs aplikacji - screne.fxml | Wczytanie pytań z pliku csv - QuestionLoader.java |
| Dokumentacja - README.md | Wyświetlanie danych na interfejsie - Controller.java |