package org.example;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.lang.Math;

public class Controller {

    @FXML
    private BorderPane pane;
    @FXML
    private Label questionLabel;
    @FXML
    private Label pointsLabel;
    @FXML
    private ToggleGroup optionsGroup;
    @FXML
    private Button submitButton;
    @FXML
    private RadioButton rb1;
    @FXML
    private RadioButton rb2;
    @FXML
    private RadioButton rb3;
    @FXML
    private RadioButton rb4;
    int currentQuestion=0;
    @FXML
    int score = 0;
    private Stage stage;
    String text = "Result: ";
    ArrayList<String> userAnswers = new ArrayList<>();
    List<Question> questions = QuestionLoader.loadQuestions("questions.csv");
    final int PASS_POINTS = (int) Math.ceil(questions.size()/2.0);

    @FXML
    public void initialize(){
        Collections.shuffle(questions);

        questionLabel.setText((questions.get(currentQuestion)).getQuestion());
        pointsLabel.setText("Points: " + score);
        String option1 = questions.get(currentQuestion).getOptions().get(0);
        String option2 = questions.get(currentQuestion).getOptions().get(1);
        String option3 = questions.get(currentQuestion).getOptions().get(2);
        String option4 = questions.get(currentQuestion).getOptions().get(3);

        rb1.setText(option1);
        rb2.setText(option2);
        rb3.setText(option3);
        rb4.setText(option4);
        submitButton.setText("Next");

        submitButton.setOnAction(event -> {
            try {
                getNextQuestion(questions);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    private void checkAnswer() {
        if (score >= PASS_POINTS) {
            text = text + score + "/" + questions.size() + " = passed\n\n";
        }
        else {
            text = text + score + "/" + questions.size() + " = failed\n\n";
        }
        for (int i = 0; i < questions.size(); i++) {
            text = text + "Question nr " + (i+1) + ":\nselected answer - " + userAnswers.get(i) + "\ncorrect answer - " + questions.get(i).getCorrectAns() + "\n\n";
        }
    }

    private void getNextQuestion(List<Question> questions) throws FileNotFoundException {
        if (currentQuestion<questions.size()) {
            String selectedRB = ((RadioButton)optionsGroup.getSelectedToggle()).getText();
            userAnswers.add(selectedRB);
            String correctAnswer = questions.get(currentQuestion).getCorrectAns();
            if (selectedRB.equals(correctAnswer) ){
                score++;
                System.out.println("Correct Answer");
            }
            else {
                System.out.println("Wrong Answer");
            }
            currentQuestion++;
            if(currentQuestion<questions.size()){
                questionLabel.setText((questions.get(currentQuestion)).getQuestion());
                pointsLabel.setText("Points: " + score);
                String option1 = questions.get(currentQuestion).getOptions().get(0);
                String option2 = questions.get(currentQuestion).getOptions().get(1);
                String option3 = questions.get(currentQuestion).getOptions().get(2);
                String option4 = questions.get(currentQuestion).getOptions().get(3);

                rb1.setText(option1);
                rb2.setText(option2);
                rb3.setText(option3);
                rb4.setText(option4);
            }
            else{
                if (score >= PASS_POINTS) {
                    questionLabel.setText("Result: passed");
                    System.out.println("Congratulations! Your knowledge is amazing!");
                }
                else {
                    questionLabel.setText("Result: failed");
                    System.out.println("Try again or comeback later");
                }
                pointsLabel.setText("Points: " + score + "/" + questions.size());
                rb1.setVisible(false);
                rb2.setVisible(false);
                rb3.setVisible(false);
                rb4.setVisible(false);
                submitButton.setText("Quit");
                checkAnswer();
            }
        }
        else {
            stage = (Stage) pane.getScene().getWindow();
            stage.close();
            try (PrintWriter out = new PrintWriter("results.txt")) {
                out.println(text);
            }
        }
    }
}